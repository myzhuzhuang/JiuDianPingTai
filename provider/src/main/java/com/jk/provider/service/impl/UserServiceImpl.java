package com.jk.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.api.service.UserService;
import com.jk.provider.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Service
@Component
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public Integer count() {
        return userMapper.count();
    }
}
