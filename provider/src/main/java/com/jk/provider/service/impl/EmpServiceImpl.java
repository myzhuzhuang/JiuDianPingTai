package com.jk.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.api.model.*;
import com.jk.api.service.EmpService;
import com.jk.common.utils.Md5Util;
import com.jk.provider.mapper.EmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.stereotype.Component;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Component
public class EmpServiceImpl implements EmpService {


    @Autowired
    private EmpMapper empMapper;

    @Autowired
    private MongoTemplate mongoTemplate;


    //总台树
    @Override
    public List<EmpTreebean> querytree() {
        Integer pid=0;
        List<EmpTreebean> trees = findNodes(pid);
        return trees;
    }



    //优惠券
    @Override
    public Map findCoupon(Integer page, Integer rows,CouponBean couponBean) {
        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, Object> params = new HashMap<>();
        params.put("couponBean",couponBean);
        int count=empMapper.findCouponCount(params);
        result.put("total",count);

        params.put("start",(page-1)*rows);
        params.put("end",rows);

        List<CouponBean> coulist=empMapper.findCoupon(params);
        result.put("rows",coulist);

        return result;
    }

    //登录
    @Override
    public Map login(EmpBean empBean) {
        HashMap<String, Object> result = new HashMap<>();

        //判断账号是否存在
        EmpBean empBean2 = empMapper.finduserInfoById(empBean.getEmp_username());
        if(empBean2==null){
            result.put("code",3);
            result.put("msg", "账号或密码错误");
            return result;
        }
        //数据库的密码
        String empPwd = empBean2.getEmp_password();
        //用户传来的密码
        String emppassword = Md5Util.getMd532(empBean.getEmp_password());
        if(!empPwd.equals(emppassword)){
            result.put("code",4);
            result.put("msg", "账号或密码错误");
            return result;
        }
        //账号
        result.put("emp_name",empBean2.getEmp_username());


        result.put("code",0);
        result.put("msg", "登录成功");
        return result;
    }

    //查找订单
    @Override
    public Map findOrderManage(Integer page, Integer rows,OrderBean orderBean) {
        HashMap<String, Object> result = new HashMap<>();
        HashMap<String, Object> params = new HashMap<>();
        params.put("orderBean",orderBean);
        Integer count=empMapper.findOrderManageCount(params);
        result.put("total",count);

        params.put("start",(page-1)*rows);
        params.put("end",rows);

        List<OrderBean> orManagerlist=empMapper.findOrderManage(params);
        result.put("rows",orManagerlist);

        return result;

    }

    @Override
    public void updateOrStatus(Integer jid) {
        empMapper.updateOrStatus(jid);
    }

    @Override
    public void updateOrJuStatus(Integer jud) {

        empMapper.updateOrJuStatus(jud);
    }


    private List<EmpTreebean> findNodes(Integer pid) {
        List<EmpTreebean> trees=empMapper.querytree(pid);
        for (EmpTreebean tree : trees) {
            Integer id1 = tree.getId();
            List<EmpTreebean> children = findNodes(id1);
            tree.setChildren(children);
        }
        return trees;


    }


}
