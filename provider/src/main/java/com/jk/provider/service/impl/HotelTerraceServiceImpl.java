package com.jk.provider.service.impl;


import com.alibaba.dubbo.config.annotation.Service;
import com.jk.api.model.*;
import com.jk.api.service.HotelTerraceService;
import com.jk.provider.mapper.HotelTerraceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Component
public class HotelTerraceServiceImpl implements HotelTerraceService {
    @Autowired
    private HotelTerraceMapper hotelTerraceServiceMapper;

    //房型查看
    @Override
    public Map FindRoomHotelType(Integer page, Integer rows, HotelRoomBean hotelRoom) {
        //总返回
        HashMap<String, Object> map = new HashMap<>();
        //条件
        HashMap<String, Object> parse = new HashMap<>();
        parse.put("hotelRoom",hotelRoom);

        int count=hotelTerraceServiceMapper.HotelRoomCount(parse);
        map.put("total",count);

        parse.put("start",(page-1)*rows);
        parse.put("end",rows);

        List<HotelBean> list=hotelTerraceServiceMapper.findRoomList(parse);
        map.put("rows",list);
        return map;
    }

    //房间查看类型
    @Override
    public List<HotelRoomTypeBean> findroomlist() {
        return hotelTerraceServiceMapper.findroomlist();
    }

    //回显房型
    @Override
    public HotelRoomBean findRoomById(Integer roomid) {
        //提出来   走debugger方便
        HotelRoomBean roomById = hotelTerraceServiceMapper.findRoomById(roomid);
        return roomById;
    }

    //修改房型
    @Override
    public void updateHoteiRoom(HotelRoomBean hotelRoomBean) {

        hotelTerraceServiceMapper.updateHoteiRoom(hotelRoomBean);
    }

    //查看在线
    @Override
    public List findRoomStateList() {
        return hotelTerraceServiceMapper.findRoomStateList();
    }

    //添加房型
    @Override
    public void addHotelRoom(HotelRoomBean hotelRoomBean) {
        hotelTerraceServiceMapper.addHotelRoom(hotelRoomBean);
    }

    //查询酒店7天营业额图
    @Override
    public List queryHotelRoomMoney(Integer hotel_id) {


        List<Object> list = new ArrayList<>();

        List<Map> maps = hotelTerraceServiceMapper.queryHotelRoomMoney(hotel_id);

       String [] price= new  String[maps.size()];
       String [] date= new  String[maps.size()];

        for (int i = 0; i<maps.size(); i++) {

                price[i] =maps.get(i).get("order_price").toString();
                date[i] =maps.get(i).get("order_inDate").toString();

        }

        list.add(date);
        list.add(price);

        return  list;
    }

    //删除房型
    @Override
    public void delHotelRoom(String[] room_ids) {
        hotelTerraceServiceMapper.delHotelRoom(room_ids);
    }

     //查询在住会员
    @Override
    public Map findHotelUserList(Integer page, Integer rows, OrderBean orderBean) {
        //总返回
        HashMap<String, Object> map = new HashMap<>();
        //条件
        HashMap<String, Object> parse = new HashMap<>();
        parse.put("orderBean",orderBean);
        int count=hotelTerraceServiceMapper.findHotelUserListcount(parse);
        map.put("total",count);

        parse.put("start",(page-1)*rows);
        parse.put("end",rows);

        List<OrderBean> list=hotelTerraceServiceMapper.findHotelUserList(parse);
        map.put("rows",list);
        return map;

    }

    //查询离店会员
    @Override
    public Map findHotelUserListGo(Integer page, Integer rows, OrderBean orderBean) {
        //总返回
        HashMap<String, Object> map = new HashMap<>();
        //条件
        HashMap<String, Object> parse = new HashMap<>();
        parse.put("orderBean",orderBean);
        int count=hotelTerraceServiceMapper.findHotelUserListCountGo(parse);
        map.put("total",count);

        parse.put("start",(page-1)*rows);
        parse.put("end",rows);

        List<OrderBean> list=hotelTerraceServiceMapper.findHotelUserListGo(parse);
        map.put("rows",list);
        return map;
}

   //查询登陆的酒店信息
    @Override
    public Map FindHotel(Integer rows, Integer page, HotelBean hotelBean) {
        //总返回
        HashMap<String, Object> map = new HashMap<>();
        //条件
        HashMap<String, Object> parse = new HashMap<>();
        parse.put("hotelBean",hotelBean);
        int count = hotelTerraceServiceMapper.FindHotelCount(parse);
        map.put("total",count);
        parse.put("start",(page-1)*rows);
        parse.put("end",rows);
        List<HotelBean> list=hotelTerraceServiceMapper.FindHotel(parse);
        map.put("rows",list);

        return map;
    }
    //改酒店状态
    @Override
    public Map UpdatehotelById(Integer hotel_id, Integer flag) {
            HashMap<String, Object> map = new HashMap<>();
            HotelBean hotelBean = hotelTerraceServiceMapper.queryHotel(hotel_id);
            if (hotelBean.getHotel_typeStar() == 3) {
                map.put("code", 1);
                map.put("msg", "无权限上架");
                return map;

            }
            if (flag == 1) {
            hotelTerraceServiceMapper.UpdatehotelByIdX(hotel_id);
            map.put("code", 0);
            map.put("msg", "下架成功");
            return map;
            }
            if (flag == 2) {
                hotelTerraceServiceMapper.UpdatehotelByIdS(hotel_id);
                map.put("code", 0);
                map.put("msg", "上架成功");
                return map;
            }
        map.put("code", 0);
        map.put("msg", "异常");
        return map;

    }


    //查询酒店30天营业额图
    @Override
    public List queryHotelStatisticsMonth(Integer hotel_id) {
        List<Object> list = new ArrayList<>();

        List<Map> maps = hotelTerraceServiceMapper.queryHotelStatisticsMonth(hotel_id);

        String [] price= new  String[maps.size()];
        String [] date= new  String[maps.size()];

        for (int i = 0; i<maps.size(); i++) {

            price[i] =maps.get(i).get("order_price").toString();
            date[i] =maps.get(i).get("order_inDate").toString();

        }

        list.add(date);
        list.add(price);

        return  list;
    }



    //酒店平台树
    @Override
    public List<HotelTerraceTreeBean> HotelTerraceTree() {
        int  pid=0;
        List<HotelTerraceTreeBean> list = digui(pid);
        return list;
    }
    private List<HotelTerraceTreeBean> digui(int pid) {
        List<HotelTerraceTreeBean> list =hotelTerraceServiceMapper.queryHotelTerraceTree(pid);
        for (HotelTerraceTreeBean map : list) {
            Integer id = map.getId();
            List<HotelTerraceTreeBean> querytree = digui(id);
            if(querytree!=null && querytree.size()>0){
                map.setNodes(querytree);
            }else{
                map.setSelectable(true);
            }
        }
        return list;
    }



}
