package com.jk.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelStateBean;
import com.jk.api.model.HotelTypeStarBean;
import com.jk.api.service.HotelService;
import com.jk.provider.mapper.HotelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Component
public class HotelServiceImpl implements HotelService {

    @Autowired
    private HotelMapper hotelMapper;

    //酒店查询
    @Override
    public Map HotelFindList(Integer page, Integer rows, HotelBean hotelBean) {
        //总返回
        HashMap<String, Object> map = new HashMap<>();
        //条件
        HashMap<String, Object> parse = new HashMap<>();
        parse.put("hotelBean",hotelBean);

        int count=hotelMapper.HotelCount(parse);
        map.put("total",count);

        parse.put("start",(page-1)*rows);
        parse.put("end",rows);

        List<HotelBean> list=hotelMapper.findList(parse);
        map.put("rows",list);
        return map;
    }

    //查询酒店星级
    @Override
    public List<HotelTypeStarBean> hoteltypeStar() {
        return hotelMapper.hoteltypeStar();
    }

    //酒店状态查询
    @Override
    public List<HotelStateBean> hotelstate() {
        return     hotelMapper.hotelstate();
    }

    @Override
    public Map findHotelAuditTable(Integer page, Integer rows, HotelBean hotelBean) {
        //总返回
        HashMap<String, Object> map = new HashMap<>();
        //条件
        HashMap<String, Object> parse = new HashMap<>();
        parse.put("hotelBean",hotelBean);

        int count=hotelMapper.HotelAuditCount(parse);
        map.put("total",count);

        parse.put("start",(page-1)*rows);
        parse.put("end",rows);

        List<HotelBean> list=hotelMapper.findHotelAuditList(parse);
        map.put("rows",list);
        return map;
    }

    @Override
    public void updateHotelStatusOk(Integer id) {
        hotelMapper.updateHotelStatusOk(id);
    }

    @Override
    public void updateHotelStatusNo(Integer id) {
        hotelMapper.updateHotelStatusNo(id);
    }

    @Override
    public void UpdatehotelById2(Integer hotel_id, Integer flag) {
        if (flag==1){
            hotelMapper.UpdatehotelByIdX2(hotel_id);
        }
        if (flag==2){
            hotelMapper.UpdatehotelByIdS2(hotel_id);
        }
    }
}
