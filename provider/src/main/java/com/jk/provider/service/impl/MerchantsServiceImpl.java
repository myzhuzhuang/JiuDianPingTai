package com.jk.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelRegionBean;
import com.jk.api.model.MerchantsPhotoBean;
import com.jk.api.service.MerchantsService;
import com.jk.common.utils.Md5Util;
import com.jk.provider.mapper.MerchantsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Component
public class MerchantsServiceImpl implements MerchantsService {

    @Autowired
    private MerchantsMapper merchantsMapper;


    @Override
    public List<HotelRegionBean> getProviceBind(Integer pid) {

        return merchantsMapper.getProviceBind(pid);
    }

    @Override
    public Map login(HotelBean hotelBean) {

        HashMap<String, Object> result = new HashMap<>();

        //判断账号是否存在
        HotelBean hotelBean2 = merchantsMapper.finduserInfoById(hotelBean.getHotel_account());
        if(hotelBean2==null){
            result.put("code",3);
            result.put("msg", "账号或密码错误");
            return result;
        }
        //数据库的密码
        String userPwd = hotelBean2.getHotel_passWord();
        //用户传来的密码
        String userPwd2 = Md5Util.getMd532(hotelBean.getHotel_passWord()) ;
        if(!userPwd.equals(userPwd2)){
            result.put("code",4);
            result.put("msg", "账号或密码错误");
            return result;
        }
        //状态
        Integer hotel_state = hotelBean2.getHotel_state();
        if (hotel_state ==3 | hotel_state ==4){
            result.put("code",5);
            result.put("msg", "抱歉没有通过审核");
            return result;
        }

        //账号
        result.put("hotel_account",hotelBean2.getHotel_account());
        //酒店id
        result.put("hotel_id",hotelBean2.getHotel_id());
        result.put("code",0);
        result.put("msg", "登录成功");
        return result;
    }

    @Override
    public Map registerMerchantsUser(HotelBean hotelBean) {
        HashMap<String, Object> result = new HashMap<>();
        System.out.println("进入检测账号");
            //检测账号是否存在
        HotelBean hotelBean2 = merchantsMapper.finduserInfoById(hotelBean.getHotel_account());
        if(hotelBean2 !=null){
            result.put("code", 1);
            result.put("msg", "用户名已经存在");
            return result;
        }

        String md532hotelPwd = Md5Util.getMd532(hotelBean.getHotel_passWord());
        hotelBean.setHotel_passWord(md532hotelPwd);
        //增加主表
        merchantsMapper.addhotelBean(hotelBean);
        //取出图片
        String allimg = hotelBean.getAllimg();

        String[] split = allimg.split(",");
        //商品图片
        ArrayList<MerchantsPhotoBean> merchantsPhotoBean = new ArrayList<>();
        Integer hotel_id = hotelBean.getHotel_id();
        for (String s : split) {
            MerchantsPhotoBean merchantsPhotoBean1 = new MerchantsPhotoBean();
            merchantsPhotoBean1.setHotel_id(hotel_id);
            merchantsPhotoBean1.setMerchantsPhoto(s);
            merchantsPhotoBean.add(merchantsPhotoBean1);
        }
        //增加图片
        merchantsMapper.addmerchantsPhotoBean(merchantsPhotoBean);
        result.put("code", 0);
        result.put("msg", "注册成功");

        return result;
    }
}
