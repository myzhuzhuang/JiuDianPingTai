package com.jk.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jk.api.model.UserBean;
import com.jk.api.model.UserLevelBean;
import com.jk.api.service.PhoneService;
import com.jk.common.common.ConstantConf;
import com.jk.common.utils.JwtUtils;
import com.jk.provider.mapper.OrderMapper;
import com.jk.provider.mapper.PhoneMapper;
import org.apache.catalina.connector.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Service
@Component
public class PhoneServiceImpl implements PhoneService {
    @Autowired
    private PhoneMapper phoneMapper;
    @Autowired
    private OrderMapper orderMapper;

    //通过用户id获取用户信息
    @Override
    public UserBean getUserInfo(Integer id) {
        return phoneMapper.getUserInfo(id);
    }

    //查询用户级别名称
    @Override
    public UserLevelBean findUserLevelBean(int id) {
        return orderMapper.findUserLevelBean(id);
    }

    //通过用户账号获取用户信息
    @Override
    public UserBean getUserInfoByAccount(String account) {
        return phoneMapper.getUserInfoByAccount(account);
    }
}
