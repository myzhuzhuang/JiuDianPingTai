package com.jk.provider.service.impl;

import com.alibaba.dubbo.config.annotation.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jk.api.model.*;
import com.jk.api.service.OrderService;
import com.jk.provider.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Component
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private MongoTemplate mongoTemplate;


    /**
     * 订单查询
     * @param page
     * @param rows
     * @param orderBean
     * @return
     */
    @Override
    public Map orderQuiryList(Integer page, Integer rows, OrderBean orderBean) {
        //构造总返回
        HashMap<String, Object> result  = new HashMap<>();
        //构建搜索map
        HashMap<String, Object> params  = new HashMap<>();
        params.put("orderBean",orderBean);
        //查询总数量
        int count=orderMapper.findOrderCount(params);
        //查询分页列表
        params.put("start",(page-1)*rows);
        params.put("end",rows);
        List<OrderBean> orders = orderMapper.findOrderList(params);
        result.put("total",count);
        result.put("rows",orders);
        return result;
    }

    /**
     * 订单类型状态动态
     * @return
     */
    @Override
    public List<HotelTypeBean> findlevel() {
        return orderMapper.findlevel();
    }

    /**
     * 会员等级列表
     * @param page
     * @param rows
     * @param userScoreLevelBean
     * @return
     */
    @Override
    public Map scoreQueryList(Integer page, Integer rows, UserScoreLevelBean userScoreLevelBean) {
        //构造总返回
        HashMap<String, Object> result  = new HashMap<>();
        //构建搜索map
        HashMap<String, Object> params  = new HashMap<>();
        params.put("userScoreLevelBean",userScoreLevelBean);
        //查询总数量
        int count=orderMapper.findScoreCount(params);
        //查询分页列表
        params.put("start",(page-1)*rows);
        params.put("end",rows);
        List<UserScoreLevelBean> scores =  orderMapper.findScoreList(params);

        for (UserScoreLevelBean score : scores) {
            if (score.getUserLevel_score()<1000){
                int id=1;
                UserLevelBean leve =  orderMapper.findUserLevelBean(id);
                score.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (score.getUserLevel_score()>=1000 && score.getUserLevel_score()<2000){
                int id=2;
                UserLevelBean leve =  orderMapper.findUserLevelBean(id);
                score.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (score.getUserLevel_score()>=2000 && score.getUserLevel_score()<3000){
                int id=3;
                UserLevelBean leve =  orderMapper.findUserLevelBean(id);
                score.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }if (score.getUserLevel_score()>=3000 && score.getUserLevel_score()<4000){
                int id=4;
                UserLevelBean leve =  orderMapper.findUserLevelBean(id);
                score.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (score.getUserLevel_score()>=4000 && score.getUserLevel_score()<5000){
                int id=5;
                UserLevelBean leve =  orderMapper.findUserLevelBean(id);
                score.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (score.getUserLevel_score()>5000){
                int id=6;
                UserLevelBean leve =  orderMapper.findUserLevelBean(id);
                score.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }

        }

        result.put("total",count);
        result.put("rows",scores);
        return result;
    }

    /**
     * 在缓存中查询出积分的日志
     * @param page
     * @param rows
     * @param userScoreLevelBean
     * @return
     */
    @Override
    public Map findLevelList(Integer page, Integer rows, LogBean logBean) {
        HashMap<String, Object> result  = new HashMap<>();
        Query query = new Query(); //用mongodb
        if (logBean.getMethodName()!=null && logBean.getMethodName()!=""){
            query.addCriteria(Criteria.where("methodName").is(logBean.getMethodName()));
        }
        //判断两个时间是否为空
       /* if (logBean.getStartTime()!=null && logBean.getEndTime() !=null ) {
            query.addCriteria(Criteria.where("createTime").gte(logBean.getStartTime()).lte(logBean.getEndTime()));
        }
        if (logBean.getStartTime()!=null && logBean.getEndTime() ==null ) {

            query.addCriteria(Criteria.where("createTime").gte(logBean.getStartTime()));
        }
        if (logBean.getStartTime()==null && logBean.getEndTime() !=null ) {

            query.addCriteria(Criteria.where("createTime").lte(logBean.getEndTime()));

        }*/
      long count=mongoTemplate.count(query,LogBean.class);
        result.put("total",count);
        query.skip((page-1)*rows);
        query.limit(rows);
        List<LogBean>  find = mongoTemplate.find(query,LogBean.class);
        result.put("rows",find);
        return result;
    }
}
