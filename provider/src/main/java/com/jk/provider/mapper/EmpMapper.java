package com.jk.provider.mapper;

import com.jk.api.model.CouponBean;
import com.jk.api.model.EmpBean;
import com.jk.api.model.EmpTreebean;
import com.jk.api.model.OrderBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Mapper
@Component
public interface EmpMapper {


    @Select("select * from t_emptree where pid=#{value}")
    List<EmpTreebean> querytree(Integer pid);


    Integer findCouponCount(HashMap<String, Object> params);

    List<CouponBean> findCoupon(HashMap<String, Object> params);


    @Select("select * from t_emp  e where e.emp_username=#{value}")
    EmpBean finduserInfoById(String emp_username);


    List<OrderBean> findOrderManage(HashMap<String, Object> params);

    @Update("update t_order tor set tor.order_statusid =4 where order_id=#{value}")
    void updateOrStatus(Integer jid);
    @Update("update t_order tor set tor.order_statusid =5 where order_id=#{value}")
    void updateOrJuStatus(Integer jud);

    Integer findOrderManageCount(HashMap<String, Object> params);
}
