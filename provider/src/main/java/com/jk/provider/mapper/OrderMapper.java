package com.jk.provider.mapper;

import com.jk.api.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface OrderMapper {
    int findOrderCount(HashMap<String, Object> params);

    List<OrderBean> findOrderList(HashMap<String, Object> params);

    @Select("select * from t_hotel_type")
    List<HotelTypeBean> findlevel();

    int findScoreCount(HashMap<String, Object> params);

    List<UserScoreLevelBean> findScoreList(HashMap<String, Object> params);

    @Select("select * from t_leve t WHERE t.userLevel_levelId=#{value}")
    UserLevelBean findUserLevelBean(int id);
}
