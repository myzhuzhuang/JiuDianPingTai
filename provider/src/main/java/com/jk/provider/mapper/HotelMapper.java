package com.jk.provider.mapper;

import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelStateBean;
import com.jk.api.model.HotelTypeStarBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface HotelMapper {

    //查询酒店数据列表条数
    int HotelCount(HashMap<String, Object> parse);

    //查询酒店数据列表
    List<HotelBean> findList(HashMap<String, Object> parse);

    //查询酒店星级
    List<HotelTypeStarBean> hoteltypeStar();

    //查询酒店状态
    List<HotelStateBean> hotelstate();

    //酒店审核平台管理
    int HotelAuditCount(HashMap<String, Object> parse);
    //酒店审核联查图片  列表查询
    List<HotelBean> findHotelAuditList(HashMap<String, Object> parse);
    //审核修改酒店状态
    @Update("UPDATE  t_hotel set hotel_state=1 where hotel_id=#{value}")
    void updateHotelStatusOk(Integer id);
    //审核修改酒店状态
    @Update("UPDATE  t_hotel set hotel_state=2 where hotel_id=#{value}")
    void updateHotelStatusNo(Integer id);

    //修改上架
    @Select("update  t_hotel th set th.hotel_typeStar=1  where  th.hotel_id=#{value}")
    void UpdatehotelByIdX2(Integer hotel_id);

    //修改下架
    @Select("update  t_hotel th set th.hotel_typeStar=3  where  th.hotel_id=#{value}")
    void UpdatehotelByIdS2(Integer hotel_id);
}
