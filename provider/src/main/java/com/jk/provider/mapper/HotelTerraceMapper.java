package com.jk.provider.mapper;

import com.jk.api.model.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface HotelTerraceMapper {
    //查询酒店平台树
    List<HotelTerraceTreeBean> queryHotelTerraceTree(int pid);

    //房型查询数量
    int HotelRoomCount(HashMap<String, Object> parse);

    //房型查询数据
    List<HotelBean> findRoomList(HashMap<String, Object> parse);

    //查询房间类型
    @Select("select  *  from t_hotel_room_type ")
    List<HotelRoomTypeBean> findroomlist();

    //回显房间类型
    @Select("select  thr.*,thrt.roomtype,th.hotel_cnName,th.hotel_enName,thrs.statename  from  t_hodel_room thr " +
            "            LEFT JOIN t_hotel_room_type thrt on thr.room_type=thrt.roomid " +
            "            LEFT JOIN t_hotel th on th.hotel_type=th.hotel_type " +
            "            LEFT JOIN  t_hotel_room_state thrs on thrs.stateid=thr.room_state   where thr.room_id=#{value}")
    HotelRoomBean findRoomById(Integer roomid);

    //修改房型
    void updateHoteiRoom(HotelRoomBean hotelRoomBean);

    //房间在线类型
    List findRoomStateList();

    //添加房型
    void addHotelRoom(HotelRoomBean hotelRoomBean);

    //查询7天营业额柱型图
    @Select("select  date_format(tor.order_inDate,'%Y-%m-%d') as order_inDate,SUM(tor.order_price) as order_price  from t_order tor where  tor.hotel_id=#{value} GROUP BY date_format(tor.order_inDate,'%Y-%m-%d') LIMIT 0,7")
    List<Map> queryHotelRoomMoney(Integer hotel_id);

    //批量删除房型
    void delHotelRoom(String[] room_ids);

    //查询30天营业额柱型图
    @Select("select  date_format(tor.order_inDate,'%Y-%m-%d') as order_inDate,SUM(tor.order_price) as order_price  from t_order tor where  tor.hotel_id=#{value} GROUP BY date_format(tor.order_inDate,'%Y-%m-%d') LIMIT 0,30")
    List<Map> queryHotelStatisticsMonth(Integer hotel_id);
    //入住会员条数
    int findHotelUserListcount(HashMap<String, Object> parse);
    //查看在住会员
    List<OrderBean> findHotelUserList(HashMap<String, Object> parse);


    //离店会员条数
    int findHotelUserListCountGo(HashMap<String, Object> parse);
    //离店会员条数
    List<OrderBean> findHotelUserListGo(HashMap<String, Object> parse);

    //查询当前酒店信息
    List<HotelBean> FindHotel(HashMap<String, Object> parse);


    //查询当前酒店数
    int FindHotelCount(HashMap<String, Object> parse);

    //商家上架
    @Select("update t_hotel set hotel_typeStar=1  where  hotel_id=#{value}")
    void UpdatehotelByIdS(Integer hotel_id);

    //商家下架
    @Select("update  t_hotel th set th.hotel_typeStar=2  where  th.hotel_id=#{value}")
    void UpdatehotelByIdX(Integer hotel_id);

    @Select("select  * from  t_hotel  where  hotel_id= #{value}")
    HotelBean queryHotel(Integer hotel_id);
}
