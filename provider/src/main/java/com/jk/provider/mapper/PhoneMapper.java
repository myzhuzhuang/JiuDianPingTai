package com.jk.provider.mapper;

import com.jk.api.model.UserBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface PhoneMapper {

    @Select("select tu.*,tsl.userLevel_score from t_scoleve tsl\n" +
            "        LEFT JOIN t_leve tl on tsl.userLevel_levelId=tl.userLevel_levelId\n" +
            "        LEFT JOIN t_user tu on tsl.user_id=tu.user_id where where tu.user_id=#{usserid}")
    UserBean getUserInfo(Integer usserid);

    @Select("select tu.*,tsl.userLevel_score from t_scoleve tsl LEFT JOIN t_user tu on tsl.user_id=tu.user_id where tu.user_account=#{account}")
    UserBean getUserInfoByAccount(String account);
}
