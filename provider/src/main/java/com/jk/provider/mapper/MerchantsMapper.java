package com.jk.provider.mapper;

import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelRegionBean;
import com.jk.api.model.MerchantsPhotoBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface MerchantsMapper {


    @Select("SELECT * FROM t_hotel t where t.hotel_account=#{value}")
    HotelBean finduserInfoById(String hotel_account);

    @Select("SELECT * from china c where pid=#{value}")
    List<HotelRegionBean> getProviceBind(Integer pid);
    
    void addmerchantsPhotoBean(ArrayList<MerchantsPhotoBean> merchantsPhotoBean);

    void addhotelBean(HotelBean hotelBean);
}
