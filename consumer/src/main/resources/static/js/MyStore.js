
//是否营业
function formateatingis(value,rowData,indexData){
	
	return value==1?'是':'否';
}

function formatOper(value,rowData,indexData){
	
	return "<a href='javascript:dican("+rowData.eating_id+","+rowData.eating_is+")'>订餐</a>";
}

function dican(id,is){
	if(is==0){
		$.messager.alert('提示信息','不开门买个锤子','warning');
		return ;
	}
	
	$("#addDialog").dialog('open').dialog('setTitle','店铺列表');
	queryVarity(id);
}


function queryVarity(id){
	
		$("#grid").datagrid({
			url:'queryVarity.do?varityid='+id,
			columns:[[
				{field:'varity_name',title:'菜单名称',width:100},
				{field:'varity_price',title:'菜单价格',width:100},
				{field:'fenshu',title:'份数',width:100,
					formatter:function(value,rowData,indexData){
						var str="<input type='button' value='-' onClick='less("+rowData.varity_id+","+rowData.varity_price+")' >";
						str +="<input type='text' id='"+rowData.varity_id+"' size='3' value='0'  >";
						str +="<input type='button' value='+' onclick='add("+rowData.varity_id+","+rowData.varity_price+")' >";
						return str;
					}
					
				},
				{field:'money',title:'此单价格',width:100,
				formatter:function(value,rowData,indexData){
					return "<span id='money"+rowData.varity_id+"'>0</span>";
				}	
				
				},
				
			]]
			
		})

	
}

//减
function less(id,price){
			
	//获取份数
	var count=$("#"+id).val()<=0?0:$("#"+id).val()-1;
	
	$("#"+id).val(count);
	changeMoney(id,price);		
}		


function add(id,price){
	
	var count=$("#"+id).val();
	count=Number(count);
	
	$("#"+id).val(count+1);
	changeMoney(id,price);		
}
	
function changeMoney(id,price){
	var count= $("#"+id).val();
	
	sumPrice=count*price;
	
	$("#money"+id).html(sumPrice)
	
}

function updatePrice(){
	var rows=$("#grid").datagrid('getRows');
	var sumPrice=0;
	for(var i=0;i<rows.length;i++){
		var vid=rows[i].varity_id;
		money=$("#money"+vid).html();
		sumPrice +=Number(money);
	}
		
	alert(sumPrice);
	$.ajax({
		url:'upatePrice.do',
		type:'post',
		data:{price:sumPrice},
		success:function(){
			$("#addDialog").dialog('close');
			 window.parent.frames.queryUser();
			 $("#grid").datagrid('reload');
			
		}
	})
	
}




function queryUser(){
	$.ajax({
		url:'./queryUser.do',
		success:function(data){
			
			for(var i=0;i<data.length;i++){
				var username=data[i].username;
				$("#username").html("<font color='red'>"+username+"</font>");
				$("#usermoney").html(data[i].usermoney);
			}
			
		},
		error:function(){
			
			alert("失败");
		}
	})
	
	
}