package com.jk.consumer.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.api.model.*;
import com.jk.api.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("order")
public class OrderController {

    @Reference(timeout = 60000)
    private OrderService orderService;

    /**
     * 订单查询
     * @param page
     * @param rows
     * @param orderBean
     * @return
     */
    @RequestMapping("orderQuiryList")
    @ResponseBody
    public Map orderQuiryList(Integer page, Integer rows, OrderBean orderBean){
        return orderService.orderQuiryList(page,rows,orderBean);
    }

    /**
     * 订单类型状态
     * @return
     */
    @RequestMapping("findlevel")
    @ResponseBody
    public List<HotelTypeBean> findlevel(){
     return orderService.findlevel();
    }

    /**
     * 会员等级列表
     * @return
     */
    @RequestMapping("scoreQueryList")
    @ResponseBody
    public Map scoreQueryList(Integer page, Integer rows,UserScoreLevelBean userScoreLevelBean ){
        return orderService.scoreQueryList(page,rows,userScoreLevelBean);
    }

    /**
     * 在缓存中查询出积分的日志
     * @param
     * @param
     * @param
     * @return
     */
    @RequestMapping("findLevelList")
    @ResponseBody
    public  Map  findLevelList (Integer page, Integer rows, LogBean logBean){
        return  orderService.findLevelList(page,rows,logBean);
    }




}
