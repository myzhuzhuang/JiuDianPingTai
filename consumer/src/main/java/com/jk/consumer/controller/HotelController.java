package com.jk.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelStateBean;
import com.jk.api.model.HotelTypeStarBean;
import com.jk.api.service.HotelService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("hotel")
public class HotelController {

    @Reference(timeout = 600000)
    private HotelService hotelService;


    /**
     *查询酒店列表
     */
    @RequestMapping("hotelFindList")
    @ResponseBody
    public Map HotelFindList(Integer page, Integer rows, HotelBean hotelBean){
        return  hotelService.HotelFindList(page,rows,hotelBean);
    }

    /**
     * 查询星级
     */
    @RequestMapping("hoteltypeStar")
    @ResponseBody
    public List<HotelTypeStarBean> hoteltypeStar(){
        return  hotelService.hoteltypeStar();
    }
    /**
     * 查询酒店状态
     */
    @RequestMapping("hotelstate")
    @ResponseBody
    public  List<HotelStateBean> hotelstate(){
        return  hotelService.hotelstate();
    }


    /**
     * 酒店审核查询 表格
     * @param page
     * @param rows
     * @param hotelBean
     * @return
     */
    @RequestMapping("findHotelAuditTable")
    @ResponseBody
    public  Map findHotelAuditTable(Integer page, Integer rows, HotelBean hotelBean){
        Map map = hotelService.findHotelAuditTable(page,rows,hotelBean);
        return  map;
    }

    /**
     * 修改酒店状态 （允许通过）
     * @param id
     */
    @RequestMapping("updateHotelStatusOk")
    @ResponseBody
    public  void updateHotelStatusOk(Integer id){

        hotelService.updateHotelStatusOk(id);
    }
    /**
     * 修改酒店状态 （不许通过）
     * @param id
     */
    @RequestMapping("updateHotelStatusNo")
    @ResponseBody
    public  void updateHotelStatusNo(Integer id){

        hotelService.updateHotelStatusNo(id);
    }

    //平台管理酒店上下架
    @RequestMapping("UpdatehotelById2")
    @ResponseBody
    public Boolean  UpdatehotelById2(Integer hotel_id,Integer flag){
        try{
            hotelService.UpdatehotelById2(hotel_id,flag);
            return  true;
        }catch (Exception e){
            e.printStackTrace();
            return  false;
        }
    }

}
