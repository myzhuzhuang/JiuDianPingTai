package com.jk.consumer.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jk.api.model.UserBean;
import com.jk.api.model.UserLevelBean;
import com.jk.api.service.PhoneService;
import com.jk.common.common.ConstantConf;
import com.jk.common.utils.JwtUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;


@Controller
@RequestMapping("phone")
public class PhoneController {

    @Reference(timeout = 500000 )
    private PhoneService phoneService;

    //手机端用户登陆
    @RequestMapping("userlogin")
    @ResponseBody
    public String  userlogin(String callback, UserBean userBean, HttpServletRequest request) throws IllegalStateException {
        HashMap<String, String> result = new HashMap<>();
        HttpSession session = request.getSession(true);
        UserBean userInfo = phoneService.getUserInfoByAccount(userBean.getUser_account());
        if(userInfo == null){
            //账号不存在
            result.put("code","1");
        }else if(!userInfo.getUser_password().equals(userBean.getUser_password())){
            //密码错误
            result.put("code","2");
        }else {
            //获取用户等级
            if (userInfo.getUserLevel_score() < 1000) {
                int id = 1;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 1000 && userInfo.getUserLevel_score() < 2000) {
                int id = 2;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 2000 && userInfo.getUserLevel_score() < 3000) {
                int id = 3;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 3000 && userInfo.getUserLevel_score() < 4000) {
                int id = 4;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 4000 && userInfo.getUserLevel_score() < 5000) {
                int id = 5;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() > 5000) {
                int id = 6;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            session.setAttribute(session.getId(), userInfo);
            //登录成功
            result.put("code", "0");
            //保存jwt认证信息
            String jsonString = JSON.toJSONString(userInfo);
            HashMap hashMap = JSONObject.parseObject(jsonString, HashMap.class);
            String jwt = JwtUtils.createJWT(userInfo.getUser_id().toString(), userInfo.getUser_account(), ConstantConf.JWT_TTL, hashMap);
            result.put("token", jwt);
        }
        String res = JSONArray.toJSONString(result);
        //用回调函数名称包裹返回数据，这样，返回数据就作为回调函数的参数传回去了
        res = callback + "(" + res + ")";
        return res;
    }

    //用户注册
    @RequestMapping("reg")
    @ResponseBody
    public String  reg(String callback, UserBean userBean,String repassword, HttpServletRequest request) throws IllegalStateException {
        HashMap<String, String> result = new HashMap<>();
        HttpSession session = request.getSession(true);
        UserBean userInfo = phoneService.getUserInfoByAccount(userBean.getUser_account());
        if(userInfo == null){
            //账号不存在
            result.put("code","1");
        }else if(!userInfo.getUser_password().equals(userBean.getUser_password())){
            //密码错误
            result.put("code","2");
        }else {
            if (userInfo.getUserLevel_score() < 1000) {
                int id = 1;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 1000 && userInfo.getUserLevel_score() < 2000) {
                int id = 2;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 2000 && userInfo.getUserLevel_score() < 3000) {
                int id = 3;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 3000 && userInfo.getUserLevel_score() < 4000) {
                int id = 4;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() >= 4000 && userInfo.getUserLevel_score() < 5000) {
                int id = 5;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            if (userInfo.getUserLevel_score() > 5000) {
                int id = 6;
                UserLevelBean leve = phoneService.findUserLevelBean(id);
                userInfo.setUserLevel_levelName(leve.getUserLevel_levelName()); // 把string的字段拿过来赋值
            }
            session.setAttribute(session.getId(), userInfo);
            //登录成功
            result.put("code", "0");
            //保存jwt认证信息
            String jsonString = JSON.toJSONString(userInfo);
            HashMap hashMap = JSONObject.parseObject(jsonString, HashMap.class);
            String jwt = JwtUtils.createJWT(userInfo.getUser_id().toString(), userInfo.getUser_account(), ConstantConf.JWT_TTL, hashMap);
            result.put("token", jwt);
        }
        String res = JSONArray.toJSONString(result);
        //用回调函数名称包裹返回数据，这样，返回数据就作为回调函数的参数传回去了
        res = callback + "(" + res + ")";
        return res;
    }

}
