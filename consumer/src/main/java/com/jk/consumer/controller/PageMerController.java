package com.jk.consumer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("page")
public class PageMerController {

    /**
     * 酒店前台登录注册跳转
     * @return
     */
    @RequestMapping("tohotelRegistered")
    public  String tohotelRegistered(){

        return "hotelRegistered";
    }

    /**
     * 酒店注册
     * @return
     */
    @RequestMapping("registerhostel")
    public String registerhostel(){

        return "registerhostel";
    }

}
