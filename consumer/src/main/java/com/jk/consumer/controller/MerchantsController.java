package com.jk.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelRegionBean;
import com.jk.api.service.MerchantsService;
import com.jk.common.utils.OSSClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("merchant")
public class MerchantsController {

    @Reference(timeout = 600000)
    private MerchantsService merchantsService;

    //图片上传
    @RequestMapping("upload")
    @ResponseBody
    public Map upload(MultipartFile imgfile, HttpServletRequest request) throws IOException {
        OSSClientUtil ossClient = new OSSClientUtil();
        String name = ossClient.uploadImg2Oss(imgfile);
        String fileUpload = ossClient.getImgUrl(name);
        HashMap<String, Object> result = new HashMap<>();
        result.put("img", fileUpload);
        return result;
    }

    /**
     * 省级获取
     * @return
     */
    @RequestMapping("getProviceBind")
    @ResponseBody
    public  List<HotelRegionBean> getProviceBind(Integer pid){

        List<HotelRegionBean> plist=  merchantsService.getProviceBind(pid);

        return  plist;
    }



    /**
     * 登录  session 不能传参 因为session 不能被序列化
     * @return
     */
    @RequestMapping("login")
    @ResponseBody
    public  Map  login(HotelBean hotelBean,HttpSession session){

        Map login = merchantsService.login(hotelBean);
        session.setAttribute(session.getId(),login);
        return  login;
    }

   /* //注销酒店用户
    @RequestMapping("removeHotel")
    public  String removeHotel(HttpSession session){
       session.removeAttribute(session.getId());
       return  "page/tohotelRegistered";
    }*/
    /**
     * 商家注册
     * @return
     */
    @RequestMapping("registerMerchantsUser")
    @ResponseBody
    public  Map registerMerchantsUser(HotelBean hotelBean){
        Map registerMerchants=   merchantsService.registerMerchantsUser(hotelBean);

        return  registerMerchants;
    }


}
