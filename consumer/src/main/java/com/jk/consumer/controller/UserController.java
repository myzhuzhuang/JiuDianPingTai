package com.jk.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.api.model.HotelBean;
import com.jk.api.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("user")
public class UserController {

    @Reference
    private UserService userService;

    @RequestMapping("count")
    @ResponseBody
    public Integer Count(){
        return userService.count();
    }

    /**
     * 商家登录
     */
    @RequestMapping("savemerchant")
    @ResponseBody
    public  void savemerchant(HotelBean hotelBean){


    }


}
