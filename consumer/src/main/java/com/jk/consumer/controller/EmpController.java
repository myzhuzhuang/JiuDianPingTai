package com.jk.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.api.model.CouponBean;
import com.jk.api.model.EmpBean;
import com.jk.api.model.EmpTreebean;

import com.jk.api.model.OrderBean;
import com.jk.api.service.EmpService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("emp")
public class EmpController {

   @Reference(timeout = 45000)
    private EmpService empService;


   //拒单
    @RequestMapping("updateOrJuStatus")
    @ResponseBody
    public void updateOrJuStatus(Integer jud){

        empService.updateOrJuStatus(jud);
    }

   //接单
    @RequestMapping("updateOrStatus")
    @ResponseBody
    public void updateOrStatus(Integer jid){

        empService.updateOrStatus(jid);
    }


   //优惠券
    @RequestMapping("findCoupon")
    @ResponseBody
    public Map findCoupon(Integer page,Integer rows,CouponBean couponBean){

        return empService.findCoupon(page,rows,couponBean);
    }


    //订单
    @RequestMapping("findOrderManage")
    @ResponseBody
    public Map findOrderManage(Integer page, Integer rows,OrderBean orderBean){

        return empService.findOrderManage(page,rows,orderBean);
    }


    //登录
    @RequestMapping("empLogin")
    @ResponseBody
    public Map login(EmpBean empBean, HttpSession session) {
       Map login= empService.login(empBean);
        session.setAttribute(session.getId(),login);
        return login;
    }
    //平台
    @RequestMapping("querytree")
    @ResponseBody
    public List<EmpTreebean> querytree(){

       return empService.querytree();
    }

}
