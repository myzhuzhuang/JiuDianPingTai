package com.jk.consumer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("pageorder")
public class OrderPageController {

    @RequestMapping("order")
    public String order(){
        return "orderlist";
    }
    @RequestMapping("scoreleve")
    public String scoreleve(){
        return "scoreleve";
    }

    @RequestMapping("mongodblist")
    public String mongodblist(){
        return "mongodblist";
    }
}
