package com.jk.consumer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("pagehotel")
public class PageHotelController {

    //查询酒店跳转
    @RequestMapping("pageHotelFindList")
    public  String  pageHotelFindList(){
        return "hotel/findHotelList";
    }


    //查询酒店页面树跳转
    @RequestMapping("findTreeHotelList")
    public  String  FindTreeHotelList(){
        return "hotelTerrace/findTreeHotelList";
    }


    //房型查看跳转
    @RequestMapping("roomtype")
    public  String  roomtype(){
        return  "hotelTerrace/roomtype";
    }

    //修改房型跳转页面
    @RequestMapping("updateRoom")
    public  String  updateRoom(){
        return  "hotelTerrace/updateRoom";
    }

    //统计最近7天营业额跳转页面
    @RequestMapping("queryHotelStatistics")
    public  String  queryHotelStatistics(){
        return  "hotelTerrace/queryHotelStatistics";
    }
    //统计最近30天营业额跳转页面
    @RequestMapping("queryHotelStatisticsMonth")
    public  String  queryHotelStatisticsMonth(){
        return  "hotelTerrace/queryHotelStatisticsMonth";
    }

    //发布添加房型跳转
    @RequestMapping("addHotelRoom")
    public  String  addHotelRoom(){
        return "hotelTerrace/addHotelRoom";
    }

    //  酒店管理界面（平台端）
    @RequestMapping("auditHotel")
    public  String auditHotel(){
        return  "hotel/auditHotel";
    }

    //跳转入住会员
    @RequestMapping("findHotelUserList")
    public  String findHotelUserList(){
        return  "hotelTerrace/findHotelUserList";
    }

    //跳转离店会员
    @RequestMapping("findHotelUserListGo")
    public  String  findHotelUserListGo(){
        return  "hotelTerrace/findHotelUserListGo";
    }

}
