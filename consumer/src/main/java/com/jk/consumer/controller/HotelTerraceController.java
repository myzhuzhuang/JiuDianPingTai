package com.jk.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.jk.api.model.*;
import com.jk.api.service.HotelTerraceService;
import com.jk.common.utils.FileUtil;
import com.jk.common.utils.OSSClientUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("hotelterrace")
public class HotelTerraceController {

    @Reference(timeout =66666)
    private HotelTerraceService hotelTerraceService;

    //酒店树查询
    @RequestMapping("hotelTerraceTree")
    @ResponseBody
    public List<HotelTerraceTreeBean> hotelTerraceTree(){
        return  hotelTerraceService.HotelTerraceTree();
    }

    //房型查询
    @RequestMapping("FindRoomHotelType")
    @ResponseBody
    public Map FindRoomHotelType(Integer page, Integer rows, HotelRoomBean hotelRoom){
        return  hotelTerraceService.FindRoomHotelType(page,rows,hotelRoom);
    }

    //查询房间类型
    @RequestMapping("findroomlist")
    @ResponseBody
    public  List<HotelRoomTypeBean> findroomlist(){
        return  hotelTerraceService.findroomlist();
    }

    //回显房型
    @RequestMapping("findRoomById")
    @ResponseBody
    public HotelRoomBean findRoomById(Integer roomid){
        //提出来   走debugger方便
        HotelRoomBean roomById = hotelTerraceService.findRoomById(roomid);
        return roomById;
    }

    //图片
    @RequestMapping("uploadimg")
    @ResponseBody
    public Map upload(MultipartFile imgfile, HttpServletRequest request) throws IOException {
        OSSClientUtil ossClient = new OSSClientUtil();
        String name = ossClient.uploadImg2Oss(imgfile);
        String fileUpload = ossClient.getImgUrl(name);
        HashMap<String, Object> result = new HashMap<>();
        result.put("img", fileUpload);
        return result;
    }


    //房型状态
    @RequestMapping("findRoomStateList")
    @ResponseBody
    public  List<HotelRoomStateBean> findRoomStateList(){
       return  hotelTerraceService.findRoomStateList();
    }

    //修改房型
    @RequestMapping("updateHoteiRoom")
    @ResponseBody
    public Boolean  updateHoteiRoom(HotelRoomBean hotelRoomBean){
        try{
            hotelTerraceService.updateHoteiRoom(hotelRoomBean);
            return  true;
        }catch (Exception e){
            e.printStackTrace();
            return  false;
        }
    }

    //添加房型
    @RequestMapping("addHotelRoom")
    @ResponseBody
    public  Boolean addHotelRoom(HotelRoomBean hotelRoomBean){
        try{
            hotelTerraceService.addHotelRoom(hotelRoomBean);
            return  true;
        }catch (Exception e){
            e.printStackTrace();
            return  false;
        }
    }

    //查询7天酒店营业额图
    @RequestMapping("queryHotelRoomMoney")
    @ResponseBody
    public  List queryHotelRoomMoney(Integer hotel_id){
        return  hotelTerraceService.queryHotelRoomMoney(hotel_id);
    }

    //查询30天酒店营业额图
    @RequestMapping("queryHotelStatisticsMonth")
    @ResponseBody
    public  List queryHotelStatisticsMonth(Integer hotel_id){
        return  hotelTerraceService.queryHotelStatisticsMonth(hotel_id);
    }

    //删除酒店房型
    @RequestMapping("delHotelRoom")
    @ResponseBody
    public  Boolean  delHotelRoom(String []room_ids){
        try{
            hotelTerraceService.delHotelRoom(room_ids);
            return  true;
        }catch (Exception e){
            e.printStackTrace();
            return  false;
        }
    }

    //查询在住酒店会员
    @RequestMapping("findHotelUserList")
    @ResponseBody
    public  Map findHotelUserList(Integer  page,Integer rows,OrderBean  orderBean){
        return   hotelTerraceService.findHotelUserList(page,rows,orderBean);
    }
    //查询离店酒店会员
    @RequestMapping("findHotelUserListGo")
    @ResponseBody
    public  Map findHotelUserListGo(Integer  page,Integer rows,OrderBean  orderBean){
        return   hotelTerraceService.findHotelUserListGo(page,rows,orderBean);
    }

    //查询登陆的酒店信息
    @RequestMapping("FindHotel")
    @ResponseBody
    public   Map  FindHotel(Integer rows,Integer page,HotelBean  hotelBean){
        return   hotelTerraceService.FindHotel(rows,page,hotelBean);
    }

    //上下架酒店
    @RequestMapping("UpdatehotelById")
    @ResponseBody
    public Map  UpdatehotelById(Integer hotel_id,Integer flag){


           return  hotelTerraceService.UpdatehotelById(hotel_id,flag);

    }
}
