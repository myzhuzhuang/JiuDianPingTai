package com.jk.consumer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("emppage")
public class EmpPageController {

    //优惠券
    @RequestMapping("toCoupon")
    public String toCoupon(){

        return "coupon";
    }

    //订单管理
    @RequestMapping("toOrderManage")
    public String toOrderManage(){

        return "ordermanage";
    }

    //统计
    @RequestMapping("tostatistics")
    public String tostatistics(){

        return "tongji";
    }


    //商家酒店订单管理
    @RequestMapping("toHotelOrder")
    public String toHotelOrder(){
        return "hotelOrder";
    }

    //登录页面
    @RequestMapping("toEmpLogin")
    public String toEmpLogin(){

        return "login";
    }


    //总台页面
    @RequestMapping("toMain")
    public String toMain(){

        return "main";
    }

}
