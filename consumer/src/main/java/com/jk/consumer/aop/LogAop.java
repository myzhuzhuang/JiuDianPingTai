package com.jk.consumer.aop;

import com.alibaba.fastjson.JSON;
import com.jk.api.model.LogBean;
import com.jk.common.common.CommonConf;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import sun.util.calendar.BaseCalendar;

import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.Date;

@Aspect
@Component
public class LogAop {


    @Autowired
    private KafkaTemplate kafkaTemplate;
    //此次用的是注解方式，可以用包名达到相同的效果
    @Pointcut(value = "execution(* com.jk.consumer.controller.*.*(..))")
    public void logCut(){}
    //指向切点
    @AfterReturning(pointcut = "logCut()", returning = "obj")
    public void saveLog(JoinPoint joinPoint,Object obj){
        System.out.println("来日志了");
        LogBean logBean = new LogBean();
        String methodName = joinPoint.getSignature().getName();//获取类名

        String className = joinPoint.getTarget().getClass().getSimpleName();//获取方法名
        Object[] args = joinPoint.getArgs();
        StringBuffer requestParams = new StringBuffer();
        for (int i = 0; i < args.length; i++) {
            requestParams.append("第"+i+"个参数=").append(args[i]);
        }

        String responseParams=obj==null ? "" :obj.toString();//返回参数
        logBean.setClassName(className);
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        logBean.setCreateTime(dateString);
        logBean.setMethodName(methodName);
        logBean.setRequestParams(requestParams.toString());
        logBean.setResponseParams(responseParams);
        String jsonString = JSON.toJSONString(logBean);
        //发送消息到kafka中
        kafkaTemplate.send(CommonConf.LOG_ORDER,jsonString);
    }

}