package com.jk.api.service;

import com.jk.api.model.*;
;
import java.util.List;
import java.util.Map;

public interface OrderService {
    Map orderQuiryList(Integer page, Integer rows, OrderBean orderBean);

    List<HotelTypeBean> findlevel();

    Map scoreQueryList(Integer page, Integer rows, UserScoreLevelBean userScoreLevelBean);


    Map findLevelList(Integer page, Integer rows, LogBean logBean);

}
