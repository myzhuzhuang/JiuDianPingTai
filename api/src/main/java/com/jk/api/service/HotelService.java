package com.jk.api.service;

import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelStateBean;
import com.jk.api.model.HotelTypeStarBean;

import java.util.List;
import java.util.Map;

public interface HotelService {
    //酒店查询列表
    Map HotelFindList(Integer page, Integer rows, HotelBean hotelBean);

    //酒店星级查询
    List<HotelTypeStarBean> hoteltypeStar();

    //酒店状态查询
    List<HotelStateBean> hotelstate();

    Map findHotelAuditTable(Integer page, Integer rows, HotelBean hotelBean);

    void updateHotelStatusOk(Integer id);

    void updateHotelStatusNo(Integer id);

    //平台管理酒店上下架
    void UpdatehotelById2(Integer hotel_id, Integer flag);
}
