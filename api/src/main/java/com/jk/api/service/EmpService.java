package com.jk.api.service;


import com.jk.api.model.CouponBean;
import com.jk.api.model.EmpBean;
import com.jk.api.model.EmpTreebean;
import com.jk.api.model.OrderBean;


import java.util.List;
import java.util.Map;


public interface EmpService {


   List<EmpTreebean> querytree();


    Map findCoupon(Integer page, Integer rows,CouponBean couponBean);

    Map login(EmpBean empBean);

    Map findOrderManage(Integer page, Integer rows, OrderBean orderBean);

    void updateOrStatus(Integer jid);

    void updateOrJuStatus(Integer jud);
}
