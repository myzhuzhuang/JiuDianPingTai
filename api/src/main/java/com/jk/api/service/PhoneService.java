package com.jk.api.service;

import com.jk.api.model.UserBean;
import com.jk.api.model.UserLevelBean;


public interface PhoneService {

    UserBean getUserInfo(Integer id);

    UserLevelBean findUserLevelBean(int id);

    UserBean getUserInfoByAccount(String account);
}
