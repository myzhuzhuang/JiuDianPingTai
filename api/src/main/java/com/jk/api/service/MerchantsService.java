package com.jk.api.service;

import com.jk.api.model.HotelBean;
import com.jk.api.model.HotelRegionBean;

import java.util.List;
import java.util.Map;

public interface MerchantsService {


    List<HotelRegionBean> getProviceBind(Integer pid);

    Map login(HotelBean hotelBean);

    Map registerMerchantsUser(HotelBean hotelBean);
}
