package com.jk.api.service;

import com.jk.api.model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface HotelTerraceService {

    List<HotelTerraceTreeBean> HotelTerraceTree();

    Map FindRoomHotelType(Integer page, Integer rows, HotelRoomBean hotelRoom);

    List<HotelRoomTypeBean> findroomlist();

    //回显房型
    HotelRoomBean findRoomById(Integer roomid);

    //修改房型
    void updateHoteiRoom(HotelRoomBean hotelRoomBean);

    //房型在线状态
    List findRoomStateList();

    //发布房型
    void addHotelRoom(HotelRoomBean hotelRoomBean);

    //查询7天酒店营业额图
    List queryHotelRoomMoney(Integer hotel_id);

    //删除房型
    void delHotelRoom(String[] room_ids);

    //查询30天酒店营业额图
    List queryHotelStatisticsMonth(Integer hotel_id);

    //查询入住酒店会员
    Map findHotelUserList(Integer page, Integer rows, OrderBean orderBean);

    //查询离店酒店会员
    Map findHotelUserListGo(Integer page, Integer rows, OrderBean orderBean);

    //查询登陆的酒店信息
    Map FindHotel(Integer rows, Integer page, HotelBean hotelBean);

    //酒店修改状态
    Map UpdatehotelById(Integer hotel_id, Integer flag);
}
