package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class EmpBean implements Serializable {
    private Integer emp_id;//
    private String  emp_username;//用户名
    private String  emp_password;//密码

}
