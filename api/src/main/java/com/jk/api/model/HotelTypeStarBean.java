package com.jk.api.model;


import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class HotelTypeStarBean implements Serializable {
    private Integer hotel_typeStar;  //酒店星级id
    private String hotel_typeStarName; //酒店星级等级
}
