package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class HotelStateBean implements Serializable {
    private  Integer hotel_state; //状态id
    private  String hotel_stateName; //状态名字
}
