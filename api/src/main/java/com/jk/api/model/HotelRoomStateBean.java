package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class HotelRoomStateBean implements Serializable {
    private  Integer  stateid;   //房间状态id
    private  String   statename; //房间状态

}
