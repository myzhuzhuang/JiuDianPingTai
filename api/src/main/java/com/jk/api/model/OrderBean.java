package com.jk.api.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@ToString
@Document(value ="texts" )
public class OrderBean implements Serializable {
    private Integer order_id; //id
    private String order_number;//订单号
    private Integer hotel_id;//酒店id
    private Integer user_id;//用户id
    private Integer order_statusid;//订单状态
    private String order_come; //订单来源
    private Integer order_typeid;//订单类型
    private Integer order_payStatusid;//支付状态
    private String order_inDate;//入住时间
    private String order_outDate;//离店时间
    private Double order_price;//订单价格

    //关联的临时字段

    private String hotel_cnName;//酒店中文名称关联

    private String  user_nickName;//用户昵称

    private String  paystatus_name;//支付状态关联

    private String  status_name; //订单状态关联

    private String hotel_typeName; //酒店类型名称

    private String startDate;//入住时间区间

    private String endDate;

    private String user_account;//用户电话


    //临时字段
    private String  room_area;    //房间面积
    private String user_realName;//真实姓名
    private  String   roomtype; //房间类型

}
