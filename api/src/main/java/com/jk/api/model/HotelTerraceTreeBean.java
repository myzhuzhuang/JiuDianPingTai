package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class HotelTerraceTreeBean implements Serializable {
    private  Integer   id;
    private  String    text;
    private  String    path;
    private  Integer   pid;
    private  Boolean selectable;
    private  List<HotelTerraceTreeBean> nodes;
}
