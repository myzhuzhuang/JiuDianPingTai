package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class UserLevelBean implements Serializable {
    //会员等级表
    private Integer userLevel_levelId;//用户级别id
    private String  userLevel_levelName;//用户级别名称
}
