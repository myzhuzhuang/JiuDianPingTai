package com.jk.api.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class EmpTreebean implements Serializable {
    private Integer id;

    private String text;

    private String url;

    private Integer pid;

    private List<EmpTreebean> children;
}
