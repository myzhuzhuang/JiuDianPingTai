package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class HotelRoomTypeBean implements Serializable {
    private  Integer  roomid;   //房间类型id
    private  String   roomtype; //房间类型

}
