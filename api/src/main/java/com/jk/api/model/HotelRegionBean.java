package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class HotelRegionBean implements Serializable {

    private static final long serialVersionUID = 3610704178850284808L;
    private  Integer id;//地区id
    private  String text;//地区名字
    private  Integer pid;//父级地区id

}
