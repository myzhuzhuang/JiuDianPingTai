package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


@Data
@ToString
public class HotelRoomBean implements Serializable {

        private static final long serialVersionUID = -8013497511682697416L;
        private Integer room_id; //房型id
        private Integer hotel_id; //酒店id
        private String  room_area;    //面积
        private Integer room_numan;   //容纳人数
        private Integer room_number; // 房间数
        private String  room_wifi;  //有无WiFi
        private Integer room_type;  //房间类型 1：大床  2：双人床  3：标间   4：主题房  5：豪华套房
        private String room_descride; //房型描述（富文本编辑器）
        private Integer ren_price;  //参考价
        private String room_img;  //房间图片
        private String room_state; //房间在线状态  1在线 2下线  3房满  4在住

        private  String   roomtype; //房间类型
        private  String hotel_cnName; //酒店中文名
        private  String hotel_enName;//酒店英文名
        private  String   statename; //房间状态
}
