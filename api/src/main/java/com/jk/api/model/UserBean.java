package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class UserBean implements Serializable {
    private Integer user_id;//用户id
    private String user_account;//手机号（账号）
    private String user_password;//用户密码
    private String user_createTime;//用户注册时间
    private String user_realName;//真实姓名，后期自己修改
    private String user_nickName;//用户昵称，后期自己修改
    private String user_headImg;//用户头像，后期自己修改

    //业务字段
    private Integer userLevel_score;//用户积分
    private String  userLevel_levelName;//用户级别名称
}