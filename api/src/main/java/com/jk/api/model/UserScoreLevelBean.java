package com.jk.api.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@ToString
public class UserScoreLevelBean implements Serializable {
    //会员积分等级表
    private Integer userLevel_id;//用户等级id
    private Integer user_id;//用户id
    private Integer userLevel_score;//用户积分
    private Integer userLevel_levelId;//用户级别

    private String user_realName;//真实姓名
    private String user_nickName;//昵称
    private String user_account;//手机号
    private String  userLevel_levelName;//用户级别名称

    private Integer room_type;  //房间类型 1：大床  2：双人床  3：标间   4：主题房  5：豪华套房




}
