package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class UserMarkBean implements Serializable {
    private Integer userMark_id;//收藏实体id
    private Integer user_id;//用户id
    private  Integer hotel_id;//酒店id
}
