package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class OrPayStaBean implements Serializable {

    private Integer paystatus_id;//支付状态关联
    private String  paystatus_name;


}
