package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class CouponTypeBean implements Serializable {

    private Integer couponTypeId;//优惠券id

    private String couponType;//优惠券类型 1 现金优惠券  2 折扣代金券

}
