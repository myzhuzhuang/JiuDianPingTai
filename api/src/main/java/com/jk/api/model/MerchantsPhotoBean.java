package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class MerchantsPhotoBean implements Serializable {

    private static final long serialVersionUID = -2300943443930493738L;

    private  Integer merchants_Id; //主键id
    private  Integer hotel_id;      //酒店id
    private  String merchantsPhoto; //上传图片存放位置


}
