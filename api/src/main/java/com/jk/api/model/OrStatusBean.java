package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class OrStatusBean implements Serializable {

    private Integer status_id;//订单状态关联
    private String  status_name;


}
