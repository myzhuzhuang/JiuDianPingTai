package com.jk.api.model;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

@Data
@ToString
@Document(value = "order-log")
public class LogBean implements Serializable {
    private String id;
    private String className;
    private String methodName;
    private String requestParams;
    private String responseParams;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String createTime;

    @Transient
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String startTime;
    @Transient
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String endTime;
}
