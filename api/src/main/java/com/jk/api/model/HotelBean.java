package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


@Data
@ToString
public class HotelBean implements Serializable {

    private static final long serialVersionUID = 1233555492587310490L;
    private  Integer hotel_id;//酒店id
    private  String hotel_cnName; //酒店中文名
    private  String hotel_enName;//酒店英文名
    private  String hotel_pinYin;//拼音简码
    private  Integer hotel_type;//酒店类型  1商务型，2会议型，3经济型，4快捷型
    private  Integer hotel_typeStar;//酒店星级  1一 2二 3三 4四 5五
    private  Integer hotel_provinceId;//省份id
    private  String hotel_provinceName;//省份名字
    private  Integer hotel_citId;//城市id
    private  String hotel_cityName;//城市名称
    private  Integer hotel_region;// 行政区
    private  String hotel_regionName;//行政区名称

    private  String hotel_zipCode;// 邮编
    private  Integer hotel_roomCount;// 房间数
    private  String hotel_theContact;// 酒店联系人
    private  Integer hotel_score;//酒店评分  1-5   五分最高
    private  Integer hotel_state;//酒店状态  1 上架 2 下架  3平台下架   4 未注册成功（处于被审核阶段）
    //后加的
    private String  hotel_yubalance;   //余额字段 单位：分
    private  String  hotel_address; // 酒店地址
    private  String  hotel_account; // 酒店账号
    private  String  hotel_passWord; // 酒店密码

    private String hotel_typeName; //酒店类型名称
    private String hotel_typeStarName; //酒店星级等级

    //临时字段
    private  String hotel_stateName; //状态名字
    //临时字段
    private  String allimg;  //酒店上传图片用

}
