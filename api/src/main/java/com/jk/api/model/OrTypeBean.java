package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class OrTypeBean implements Serializable {

    private Integer type_id;//订单类型关联
    private String  type_name;

}
