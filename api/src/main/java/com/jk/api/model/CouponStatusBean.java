package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class CouponStatusBean implements Serializable {

    private Integer couponStatusId;//状态id

    private String couponStatus;//状态 1 可领取 2不可领取
}
