package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class CouponBean implements Serializable {

    private Integer couponId;//优惠券id

    private Double couponPrice;//优惠券金额

    private Integer couponStatusId;//优惠券状态

    private Integer couponTypeId;//优惠券类型

    private String couponDescripe;//优惠券说明

    //临时字段
    private String couponStatus;//状态

    private String couponType;//类型
}
