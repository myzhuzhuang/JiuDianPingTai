package com.jk.api.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


@Data
@ToString
public class HotelTypeBean implements Serializable {

    private static final long serialVersionUID = -982804763678524299L;

        private Integer hotel_type;  //酒店类型id
        private String hotel_typeName; //酒店类型名称

}
