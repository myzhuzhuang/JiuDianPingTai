package com.jk.log.logconsumer.orderlistener;

import com.alibaba.fastjson.JSON;
import com.jk.api.model.OrderBean;
import com.jk.common.commonorder.CommonConfOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class AcceptOrder {

    @Autowired
    private MongoTemplate mongoTemplate;

    @KafkaListener(topics = CommonConfOrder.ORDER_TOPIC,groupId ="consumer-group" )
    public  void consumer(String message){

        OrderBean orderBean = JSON.parseObject(message, OrderBean.class);
        mongoTemplate.save(orderBean);

    }


}
