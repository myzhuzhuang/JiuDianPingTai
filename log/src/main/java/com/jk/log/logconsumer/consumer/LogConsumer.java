package com.jk.log.logconsumer.consumer;

import com.alibaba.fastjson.JSON;
import com.jk.api.model.LogBean;
import com.jk.common.common.CommonConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class LogConsumer {
    @Autowired
    private MongoTemplate mongoTemplate;
    @KafkaListener(topics = CommonConf.LOG_ORDER,groupId = "consumer-group")
    public void consumer(String message){
        LogBean logBean = JSON.parseObject(message, LogBean.class);
        mongoTemplate.save(logBean);
    }
}
